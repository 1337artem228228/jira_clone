// eslint-disable-next-line import/no-extraneous-dependencies
const { defineConfig } = require('cypress')

module.exports = defineConfig({
  video: false,
  component: {
    devServer: {
      framework: 'react',
      bundler: 'webpack'
    }
  },
  "viewportHeight": 800,
  "viewportWidth": 1440,
  "env": {
    "apiBaseUrl": "http://localhost:3000"
  },
  "e2e": {
    "integrationFolder": "cypress/tests/",
    "testFiles": ["**/*.test.spec.ts","**/*.test.spec.js"]
  },
})
