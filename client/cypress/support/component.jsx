// ***********************************************************
// This example support/component.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')


// eslint-disable-next-line import/no-extraneous-dependencies
import { mount } from '@cypress/react'

import { MemoryRouter } from 'react-router-dom'
import React from "react";
import NormalizeStyles from "../../src/App/NormalizeStyles";
import BaseStyles from "../../src/App/BaseStyles";

Cypress.Commands.add('mount', (component, options = {}) => {
    const { routerProps = { initialEntries: ['/'] }, ...mountOptions } = options

    const wrapped = <MemoryRouter {...routerProps}>
        <NormalizeStyles />
        <BaseStyles />
        {component}
    </MemoryRouter>

    return mount(wrapped, mountOptions)
})

// Example use:
// cy.mount(<MyComponent />)