

const mockUsers = [
    {
        "id": 1,
        "name": "Lord Gaben",
        "email": "gaben@jira.guest",
        "avatarUrl": "",
        "createdAt": "2024-06-07T08:37:07.891Z",
        "updatedAt": "2024-06-07T08:37:07.928Z",
        "projectId": 1
    },
]

export const defaultProjectSettings = {
    category: "software",
    createdAt: "2024-06-07T08:37:07.928Z",
    description: "Plan, track, and manage your agile and software development projects in Jira. Customize your workflow, collaborate, and release great software.",
    id: 1,
    issues: [
        createMockIssue(),
        createMockIssue(),
        createMockIssue(),
    ],
    name: "test project",
    updatedAt: "2024-06-07T08:37:07.928Z",
    url: "https://www.atlassian.com/software/jira",
    users: [...mockUsers]
}

export const currentUserMock = mockUsers[0];

export const mergeWithDefaultProject = (customSettings) => {
    return { ...defaultProjectSettings, ...customSettings };
}

function defaultIssue(){
    return {
        "id": generateRandomId(),
        "title": "Add a new issue to the backlog",
        "type": "bug",
        "status": "backlog",
        "priority": "3",
        "listPosition": 1,
        "createdAt": "2024-06-07T08:37:07.970Z",
        "updatedAt": "2024-06-07T08:37:07.970Z",
        "userIds": []
    }
}

export function createMockIssue(customSettings) {
    return { ...defaultIssue(), ...customSettings };
}

function generateRandomId(min = 1, max = 1000000) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
