import React from "react";
// eslint-disable-next-line
import Board from "./index.tsx";
import {
    createMockIssue,
    currentUserMock,
    defaultProjectSettings,
    mergeWithDefaultProject
} from "../../../cypress/mockData";

describe('Board component', () => {
    let fetchProject;
    let updateLocalProjectIssues;
    let routerProps;

    beforeEach(() => {
        fetchProject = cy.stub();
        updateLocalProjectIssues = cy.stub();
        routerProps = {
            initialEntries: ['/project/board']
        };

        cy.intercept('GET', '**/currentUser', {
            body: {
                "currentUser": currentUserMock
            }
        });
    });

    it('should mount without errors', () => {
        cy.mount(<Board project={defaultProjectSettings}
                        fetchProject={fetchProject}
                        updateLocalProjectIssues={updateLocalProjectIssues}
        />, {routerProps})
    });

    it('should render all components', () => {
        cy.mount(<Board project={defaultProjectSettings}
                        fetchProject={fetchProject}
                        updateLocalProjectIssues={updateLocalProjectIssues}
        />, {routerProps})
        cy.getBySel('board-lists').should('exist');
        cy.getBySel('board-filters').should('exist');
    });

    it('should show correct breadcrumbs', () => {
        cy.mount(<Board project={defaultProjectSettings}
                        fetchProject={fetchProject}
                        updateLocalProjectIssues={updateLocalProjectIssues}
        />, {routerProps})

        const expectedBreadcrumbs = 'Projects/test project/Kanban Board';
        cy.getBySel('breadcrumbs').should('have.text', expectedBreadcrumbs);
    });

    it('should filter by only my issues', () => {
        // mock issues data
        const matchFilterIssues = [
            createMockIssue({"userIds": [currentUserMock.id]}),
            createMockIssue({"userIds": [currentUserMock.id]}),
            createMockIssue({"userIds": [currentUserMock.id]}),
        ]
        const projectIssues = [...defaultProjectSettings.issues, ...matchFilterIssues];
        const project = mergeWithDefaultProject({issues: projectIssues})

        cy.mount(<Board project={project}
                        fetchProject={fetchProject}
                        updateLocalProjectIssues={updateLocalProjectIssues}
        />, {routerProps})

        const initialIssuesCount = project.issues.length;

        // initial state
        cy.getBySel('list-issue').should('have.length', initialIssuesCount);

        cy.getBySel('filter-my-issues').click();

        // my issues only should be shown
        cy.getBySel('list-issue').should('have.length', matchFilterIssues.length);

        cy.getBySel('filter-my-issues').click();

        // back to initial state
        cy.getBySel('list-issue').should('have.length', initialIssuesCount);
    });

    it('should filter by recently updated', () => {
        // mock issues data
        const matchFilterIssues= [
            createMockIssue({
                "updatedAt": "2024-06-10T08:37:07.970Z",
                "userIds": [currentUserMock.id]
            }),
            createMockIssue({
                "updatedAt": "2024-06-10T08:37:07.970Z",
                "userIds": [currentUserMock.id]
            })
        ]

        const projectIssues = [...defaultProjectSettings.issues, ...matchFilterIssues];
        const project = mergeWithDefaultProject({issues: projectIssues})


        cy.mount(<Board project={project}
                        fetchProject={fetchProject}
                        updateLocalProjectIssues={updateLocalProjectIssues}
        />, {routerProps})

        // initial state
        const initialIssuesCount = project.issues.length;

        cy.getBySel('list-issue').should('have.length', initialIssuesCount);
        cy.getBySel('filter-recently-updated').click();

        cy.getBySel('list-issue').should('have.length', matchFilterIssues.length);

        cy.getBySel('filter-recently-updated').click();
        cy.getBySel('list-issue').should('have.length', initialIssuesCount);
    });

    it('should merge filters only my issues and recent', () => {
        // mock issues data
        const matchFilterIssues= [
            createMockIssue({
                "updatedAt": "2024-06-10T08:37:07.970Z",
                "userIds": [currentUserMock.id]
            }),
            createMockIssue({
                "updatedAt": "2024-06-10T08:37:07.970Z",
                "userIds": [currentUserMock.id]
            })
        ]

        const projectIssues = [...defaultProjectSettings.issues, ...matchFilterIssues];
        const project = mergeWithDefaultProject({issues: projectIssues})

        cy.mount(<Board project={project}
                        fetchProject={fetchProject}
                        updateLocalProjectIssues={updateLocalProjectIssues}
        />, {routerProps})

        cy.getBySel('filter-my-issues').click();
        cy.getBySel('filter-recently-updated').click();

        cy.getBySel('list-issue').should('have.length', matchFilterIssues.length);
    })

    it('should show only selected user issues', () => {
        const matchFilterIssues= [
            createMockIssue({
                "userIds": [currentUserMock.id]
            }),
            createMockIssue({
                "userIds": [currentUserMock.id]
            })
        ]

        const otherUserMock = {
            "id": 2,
            "name": "Test user 2",
            "email": "test2@jira.guest",
            "avatarUrl": "",
            "createdAt": "2024-06-07T08:37:07.891Z",
            "updatedAt": "2024-06-07T08:37:07.928Z",
            "projectId": 1
        }

        const otherUserIssues = [
            createMockIssue({
                "userIds": [otherUserMock.id]
            }),
            createMockIssue({
                "userIds": [otherUserMock.id]
            })
        ]

        const projectIssues = [...defaultProjectSettings.issues, ...matchFilterIssues, ...otherUserIssues];
        const projectUsers = [...defaultProjectSettings.users, otherUserMock];
        const project = mergeWithDefaultProject({issues: projectIssues, users: projectUsers})

        cy.mount(<Board project={project}
                        fetchProject={fetchProject}
                        updateLocalProjectIssues={updateLocalProjectIssues}
        />, {routerProps})

        const initialIssuesCount = project.issues.length;

        cy.getBySel('list-issue').should('have.length', initialIssuesCount);
        cy.getBySel(`filter-user-${currentUserMock.id}`).click();

        cy.getBySel('list-issue').should('have.length', matchFilterIssues.length);

        // second click should clear filter
        cy.getBySel(`filter-user-${currentUserMock.id}`).click();
        cy.getBySel('list-issue').should('have.length', initialIssuesCount);

        // click on other user
        cy.getBySel(`filter-user-${otherUserMock.id}`).click();
        cy.getBySel('list-issue').should('have.length', otherUserIssues.length);

        // second click should clear filter
        cy.getBySel(`filter-user-${otherUserMock.id}`).click();
        cy.getBySel('list-issue').should('have.length', initialIssuesCount);

        // filter by both users
        cy.getBySel(`filter-user-${currentUserMock.id}`).click();
        cy.getBySel(`filter-user-${otherUserMock.id}`).click();
        cy.getBySel('list-issue').should('have.length', matchFilterIssues.length + otherUserIssues.length);
    })

    it('should clear filters', () => {
        const matchFilterIssues= [
            createMockIssue({
                "userIds": [currentUserMock.id]
            }),
            createMockIssue({
                "userIds": [currentUserMock.id]
            })
        ]
        const projectIssues = [...defaultProjectSettings.issues, ...matchFilterIssues];
        const project = mergeWithDefaultProject({issues: projectIssues})

        cy.mount(<Board project={project}
                        fetchProject={fetchProject}
                        updateLocalProjectIssues={updateLocalProjectIssues}
        />, {routerProps})

        cy.getBySel('filter-my-issues').click();
        cy.getBySel('list-issue').should('have.length', matchFilterIssues.length);

        cy.getBySel('filter-clear-all').click();
        cy.getBySel('list-issue').should('have.length', project.issues.length);
    });
})
