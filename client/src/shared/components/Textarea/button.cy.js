import React from 'react';
import Textarea from './Textarea';

describe('Textarea Component', () => {
    it('should render', () => {
        // eslint-disable-next-line react/jsx-filename-extension
        cy.mount(<Textarea />);
        cy.get('[data-cy-root]').should('exist'); // Verify the presence of the root element
        cy.get('textarea').should('exist'); // Verify the presence of the textarea element within your component
    });
});
